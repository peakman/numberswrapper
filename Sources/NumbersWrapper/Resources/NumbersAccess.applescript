on run {}
	log "Running"
	tell application "Numbers"
		log file of first document as text
	end tell
	set docWrapper to NumbersAccess
	tell docWrapper
		log its applicationName() as text
		#openNumbersDoc_("Users:scimeb:Library:Containers:com.apple.iWork.Keynote:Data:Documents:test.numbers")
		#openNumbersDoc_("Users:scimeb:Library:Containers:peakit.OilBillUI:Data:iCloud:test.numbers")
		openNumbersDoc_("Users:scimeb:temp:DO_NOT_DELETE:test.numbers")
		activateSheet_(1)
		its updateNumber:1 column:1 value:99.06
		its updateText:2 column:2 value:"abc"
		its updateReals:{3, 3} values:{101.423, 202, 303.675}
		its updateDate:1 column:1 value:"01/01/2014"
		activateSheet_(1)
		its aeSetCol:3 startAt:3 values:{999, "--MISSING VALUE--", "zzzyy", 3, 123.67}
		its aeSetVector:"R" vidx:8 startAt:1 values:{99889, missing value, "aazzzyy"}
		addRowBelow()
		log headerCount()
		its exportAsExcel:("/Users/scimeb/temp/abcd.xlsx" as Unicode text)
		its closeSaving:0 savingIn:("/Users/scimeb/temp/abcd.numbers" as Unicode text)
	end tell
end run

script NumbersAccess
	#use scripting additions
	#use framework "Foundation"
	#use framework "ASObjCExtras"
	property parent : class "NSObject"
	property doc : missing value
	property activeSheetIndex : 1
	property activeTableIndex : 1
	
	to applicationName()
		tell application "Numbers"
			return name & " version " & version
		end tell
	end applicationName
	
	to exportAsExcel:fileName
		tell application "Numbers"
			set myFile to (fileName as Unicode text)
			export my doc as Microsoft Excel to POSIX file myFile
			return 0
		end tell
	end exportAsExcel:
	
	to closeSaving:closeOpt savingIn:fileName
		tell application "Numbers"
			close my doc saving closeOpt as boolean saving in POSIX file (fileName as Unicode text)
			set my doc to missing value
			return 0
		end tell
	end closeSaving:savingIn:
	
	to makeDocument:templateName
		tell application "Numbers"
			set my doc to make new document with properties {document template:template named (templateName as text)}
		end tell
	end makeDocument:
	
	to openNumbersDoc:docPath
		tell application "Numbers"
			activate
			set my doc to open docPath as text
			if doc = missing value then
				return "bad" as text
			else
				return file of my doc as text
			end if
		end tell
	end openNumbersDoc:
	
	to activateSheetNamed:sheetName
		tell application "Numbers"
			set idx to 1
			repeat with tryname in my sheetNames()
				if tryname as text is equal to sheetName as text then
					(my activateSheet:idx)
				end if
				set idx to idx + 1
			end repeat
		end tell
	end activateSheetNamed:
	
	to addRowBelow()
		tell application "Numbers"
			tell last row of my activeTable()
				add row below
			end tell
		end tell
	end addRowBelow
	
	to aeSetVector:rowQCol vidx:vidx startAt:start values:values
		local rng, start, vals, v
		tell application "Numbers"
			if rowQCol as text = "R" then
				set rng to row (vidx as integer) of my activeTable()
			else
				set rng to column (vidx as integer) of my activeTable()
			end if
			set strt to start as integer
			set vals to values as list
			repeat with r from strt to strt + (length of vals) - 1
				set v to (item (r - strt + 1) of vals)
				if v = "--MISSING VALUE--" or v = missing value then
					clear (cell r of rng)
				else
					set value of (cell r of rng) to (item (r - strt + 1) of vals)
				end if
			end repeat
		end tell
	end aeSetVector:vidx:startAt:values:
	
	to aeSetCol:colNum startAt:start values:values
		my aeSetVector:"C" vidx:colNum startAt:start values:values
	end aeSetCol:startAt:values:
	
	to aeSetRow:rowNum startAt:start values:values
		my aeSetVector:"R" vidx:rowNum startAt:start values:values
	end aeSetRow:startAt:values:
	
	to updateReals:rowCol values:array
		local cols, rowNum, colNum, idx
		tell application "Numbers"
			set cols to (column count of my activeTable())
			set rowNum to item 1 of rowCol as integer
			set colNum to item 2 of rowCol as integer
			set idx to (((rowNum - 1) * cols) + colNum)
			set value of cell idx of my activeTable() to item 1 of array as real
		end tell
	end updateReals:values:
	
	to updateNumber:rowIdx column:colIdx value:val
		local cols, rowNum, colNum, idx
		tell application "Numbers"
			set cols to (column count of my activeTable())
			set rowNum to rowIdx as integer
			set colNum to colIdx as integer
			set idx to (((rowNum - 1) * cols) + colNum)
			set value of cell idx of my activeTable() to val as text as number
		end tell
	end updateNumber:column:value:
	
	to updateDate:rowIdx column:colIdx value:val
		local cols, rowNum, colNum, idx
		tell application "Numbers"
			set cols to (column count of my activeTable())
			set rowNum to rowIdx as integer
			set colNum to colIdx as integer
			set idx to (((rowNum - 1) * cols) + colNum)
			set value of cell idx of my activeTable() to date (val as text)
		end tell
	end updateDate:column:value:
	
	to updateText:rowIdx column:colIdx value:val
		local cols, rowNum, colNum, idx
		tell application "Numbers"
			set cols to (column count of my activeTable())
			set rowNum to rowIdx as integer
			set colNum to colIdx as integer
			set idx to (((rowNum - 1) * cols) + colNum)
			set value of cell idx of my activeTable() to val as text
		end tell
	end updateText:column:value:
	
	to activateSheet:sheetIndex
		tell application "Numbers"
			log "SheetIndex: " & sheetIndex as text
			set my activeSheetIndex to sheetIndex as text as number
		end tell
	end activateSheet:
	
	to sheetNames()
		tell application "Numbers"
			return name of every sheet of my doc
		end tell
	end sheetNames
	
	to cells()
		tell application "Numbers"
			return value of every cell of my activeTable()
		end tell
	end cells
	
	to activeSheetName()
		tell application "Numbers"
			return name of sheet activeSheetIndex of my doc
		end tell
	end activeSheetName
	
	to activeTable()
		tell application "Numbers"
			return table activeTableIndex of sheet activeSheetIndex of my doc
		end tell
	end activeTable
	
	to matrixProperties()
		tell application "Numbers"
			set tab to my activeTable()
			return {rowCount:row count of tab, columnCount:column count of tab}
		end tell
	end matrixProperties
	
	to headerCount()
		tell application "Numbers"
			return header row count of my activeTable()
		end tell
	end headerCount
	
end script

