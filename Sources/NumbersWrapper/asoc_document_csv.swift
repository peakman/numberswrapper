//
//  docwrapper_csv.swift
//  CarelineNumbersTest
//
//  Created by Steve Clarke on 13/05/2021.
//

import Foundation
import SwiftCSV


extension ASOCDocument {
    public typealias CSVHandler = ([String])  -> [Any?]
    
    public class func defaultCsvHandler(_ csvRow: [String])  -> [Any?]  {
        return csvRow
    }
    public typealias EnumerationHandler = ([String]) -> ()
    
    public class func makeEnumerationHandler(_ csvHandler: @escaping CSVHandler, rowCount: Int, asocDoc: ASOCDocument, numbersURL: URL)  -> EnumerationHandler {
        var rowIdx = 0
        func enumerationHandler(_ strings: [String])  -> () {
            let typedData = csvHandler(strings)
            let rows = asocDoc.rowCount()
            asocDoc.aeSetRow(rows - 1, startAt: 0, values: typedData)
            let xlURL = numbersURL.deletingPathExtension().appendingPathExtension("xlsx")
            if rowIdx == rowCount {
                asocDoc.exportAsExcel(xlURL.path)
                asocDoc.close(saving: true, savingIn: numbersURL.path)
            } else {
                asocDoc.addRowBelow()
                rowIdx += 1
            }
        }
        return enumerationHandler
    }
    
    public class func processCsv(_ csv: URL,
                                 numbersURL: URL,
                                 template: String ,
                                 csvHandler: @escaping CSVHandler = ASOCDocument.defaultCsvHandler ) throws -> (){
        let docURL : URL? = nil
        let asocDoc : ASOCDocument = ASOCDocument.init(doc: docURL, template: template)
        let csv = try CSV<Enumerated>(url: csv, loadColumns: false)
        let rowCount = csv.rows.count
        let enumerationHandler = Self.makeEnumerationHandler(csvHandler, rowCount: rowCount, asocDoc: asocDoc, numbersURL: numbersURL)
        try csv.enumerateAsArray(startAt: 0,rowLimit: rowCount, enumerationHandler)
    
    }
    
}
