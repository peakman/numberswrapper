//
//  asoc_row.swift
//  CreditCardStatementToQif
//
//  Created by Steve Clarke on 04/12/2014.
//  Copyright (c) 2014 Steve Clarke. All rights reserved.
//

import Foundation

open  class ASOCRow : TextOutputStreamable {
    /// Writes a textual representation of this instance into the given output
    /// stream.
    public func write<Target : TextOutputStream>(to target: inout Target) {
        target.write("Row \(index) ")
        for idx in 0..<cells.count {
            cells[idx].write(to: &target)
            target.write(" ")
        }
    }

    public let cells : [ASOCCell]
    public let index: Int
    
    required public init(_ index: Int, cells: [ASOCCell]) {
        self.cells = cells
        self.index = index
    }
    
    public subscript(col : Int) -> ASOCCell {
        return cells[col ]
    }
    
}
