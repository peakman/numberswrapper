//
//  asoc_cell.swift
//  CreditCardStatementToQif
//
//  Created by Steve Clarke on 03/12/2014.
//  Copyright (c) 2014 Steve Clarke. All rights reserved.
//

import Foundation

public enum FailureOption {
    case ABORT, WARN, QUIET
}

public enum ValidType : String {
    case NUMBER = "Number", STRING="String", DATE="Date", MONEY = "Money", BOOL = "Bool"
}

extension NSDate {
    
    func appleScriptDate() -> NSAppleEventDescriptor
    {
        var secondsSince1904 = Int64(self.timeIntervalSinceReferenceDate + kCFAbsoluteTimeIntervalSince1904)
        return NSAppleEventDescriptor(descriptorType: DescType(typeLongDateTime), bytes: &secondsSince1904, length: MemoryLayout.size(ofValue: secondsSince1904))!
    }
    
    convenience init(appleScriptDate : NSAppleEventDescriptor)
    {
        var secondsSince1904 : Int64 = 0
        withUnsafeMutablePointer(to: &secondsSince1904) {
            appleScriptDate.data.copyBytes(
                to: UnsafeMutableBufferPointer(start: $0, count: 4),
                from: 0..<4)
        }
        self.init(timeIntervalSinceReferenceDate:TimeInterval(secondsSince1904) - kCFAbsoluteTimeIntervalSince1904)
    }
    
}

extension NSAppleEventDescriptor {
    func date() -> NSDate? {
        if let dateAed = coerce(toDescriptorType: DescType(typeLongDateTime)) {
            return NSDate(appleScriptDate: dateAed )
        } else {
            return nil
        }
    }
    class func makeASOCCell(_ data:NSAppleEventDescriptor , _ row: Int, _ col: Int) -> ASOCCell {
        if let dateAed = data.coerce(toDescriptorType: DescType(typeLongDateTime)) {
            if let date = dateAed.date() {
                return ASOCCellDate(date as NSDate , row, col)
            } else {
                print("NSAppleEventDescriptor should have a date")
                abort()
            }
        } else {
            print("NSAppleEventDescriptor of unexpected type")
            abort()
        }
    }
}


public class ASOCCell : TextOutputStreamable {
    /// Writes a textual representation of this instance into the given output
    /// stream.

    public func write<Target : TextOutputStream>(to target: inout Target) {
        target.write("\(wrapped)")
    }

    struct Formatters {
        let dateFormatter : DateFormatter = DateFormatter()
        let defaultDateFormat = "dd/MM/yy"
        let numberFormatter  = NumberFormatter()
        let currencyFormatter = NumberFormatter()
        let decimalHandler = NSDecimalNumberHandler(roundingMode: NSDecimalNumber.RoundingMode.plain, scale: 2, raiseOnExactness: false,  raiseOnOverflow: true, raiseOnUnderflow: true, raiseOnDivideByZero: true)
        init() {
            dateFormatter.dateFormat = defaultDateFormat
            numberFormatter.minimumFractionDigits=2
            numberFormatter.maximumFractionDigits=2
            numberFormatter.numberStyle = NumberFormatter.Style.decimal
            currencyFormatter.minimumFractionDigits=2
            currencyFormatter.maximumFractionDigits=2
            currencyFormatter.numberStyle = NumberFormatter.Style.currency
        }
    }
    public var wrapped: AnyObject
    let row : Int
    let col : Int
    var numberFormatter :NumberFormatter { return ASOCCell.formatters().numberFormatter}
    var dateFormatter :DateFormatter { return ASOCCell.formatters().dateFormatter}
    var currencyFormatter :NumberFormatter { return ASOCCell.formatters().currencyFormatter}
    public required init(_ data: AnyObject, _ row: Int, _ col: Int) {
        wrapped = data
        self.row = row
        self.col = col
    }

    class func formatters() -> Formatters {
        struct Static {
            static let _asoc_formatters = Formatters()
        }
        return Static._asoc_formatters
    }
    class func makeCell(_ data: AnyObject, _ row: Int, _ col: Int) throws -> ASOCCell{
        let retval : ASOCCell
        if data is NSString {
            retval =  ASOCCellString(data, row,col)
        } else if data is NSDate {
            retval = ASOCCellDate(data, row,col)
        } else if data is NSAppleEventDescriptor {
            retval =  NSAppleEventDescriptor.makeASOCCell(data as! NSAppleEventDescriptor, row,col)
        } else if data is NSNumber  {
            retval = ASOCCellNumber(data, row,col)
        } else if data is NSNull {
            retval = ASOCCellNull(NSNull(), row,col)
        } else {
            fatalError("Unexpected data type from ASOC at row \(row) , col \(col), data is \(String(describing: data.classDescription))")
        }
        return retval
    }
    public func report(_ type: ValidType, fail: FailureOption) throws {
        switch fail {
        case .ABORT:
            fatalError("Cell at \(row),\(col) cannot be converted to \(type.rawValue).  It contains \(wrapped)")
        case .QUIET: break
        case .WARN: print("Cell at \(row),\(col) cannot be converted to \(type.rawValue).  It contains \(wrapped)")
        }
    }
    public func string(_ fail: FailureOption = .WARN) -> String {
        try! report(.STRING,fail:  fail)
        return ""
    }
    public func int(_ fail: FailureOption = .WARN) -> Int {
        try! report(.NUMBER,fail:  fail)
        return 0
    }
    public func bool(_ fail: FailureOption = .WARN) -> Bool {
        try! report(.NUMBER,fail:  fail)
        return false
    }
    public func double(_ fail: FailureOption = .WARN) -> Double {
        try! report(.NUMBER,fail:  fail)
        return 0
    }
    public func number(_ fail: FailureOption = .WARN) -> NSNumber {
        try! report(.NUMBER,fail:  fail)
        return 0
    }
    public func money(_ fail: FailureOption = .WARN) -> Decimal {
        try! report(.MONEY,fail:  fail)
        return 0
    }
    public func date(_ fail: FailureOption = .WARN) -> Date {
        try! report(.DATE,fail:  fail)
        return Date()
    }
    public func displayDate(format : String? = nil)  throws -> String {
        fatalError("Cell at \(row),\(col) is not a date")
    }
    public func displayMoney() throws -> String {
        fatalError("Cell at \(row),\(col) cannot be converted to money")
    }
    public func displayNumber() throws -> String {
        fatalError("Cell at \(row),\(col) cannot be converted to number")
    }
}



public class ASOCCellNull : ASOCCell {
    public required init(_ data: AnyObject, _ row: Int, _ col: Int) {
        super.init(data , row, col)
    }
}

public class ASOCCellNumber : ASOCCell {
    public required init(_ data: AnyObject, _ row: Int, _ col: Int) {
        super.init(data as!
            NSNumber , row, col)
    }
    public override func int(_ fail: FailureOption = .WARN) -> Int {
        return Int(truncating: wrapped as! NSNumber)
    }
    override public func bool(_ fail: FailureOption = .WARN) -> Bool {
        let v = Int(truncating: wrapped as! NSNumber)
        switch v {
        case 0:
            return false
        case 1:
            return true
        default:
            try! report(.BOOL,fail:  fail)
            return false
        }
    }
    override public func number(_ fail: FailureOption = .WARN) -> NSNumber {
        return wrapped as! NSNumber
    }
    override public func money(_ fail: FailureOption = .WARN) -> Decimal {
        return  Decimal(wrapped as! Double )
    }
    override public func displayMoney() -> String {
        return currencyFormatter.string(from: wrapped as! NSNumber)!
    }
    override public func displayNumber() -> String {
        return numberFormatter.string(from: wrapped as! NSNumber)!
    }
    override public func double(_ fail: FailureOption = .WARN) -> Double {
        return Double(truncating: wrapped as! NSNumber)
    }
}
public class ASOCCellString : ASOCCell {
    public required init(_ data: AnyObject, _ row: Int, _ col: Int) {
        super.init(data as! String as AnyObject , row, col)
    }
    override public func string(_ fail: FailureOption = .WARN) -> String {
        return wrapped as! String
    }
}


public class ASOCCellDate : ASOCCell {
    public required init(_ data: AnyObject, _ row: Int, _ col: Int) {
        super.init(data as! NSDate , row, col)
    }
    override public func date(_ fail: FailureOption = .WARN) -> Date {
        return (wrapped as! NSDate) as Date
    }
    func my_date() -> Date  {
        return  (wrapped as! NSDate) as Date
    }
    
    override public func displayDate(format : String? = nil) -> String {
        if let fmt = format {
            dateFormatter.dateFormat = fmt
        } else {
            dateFormatter.dateFormat = ASOCCell.formatters().defaultDateFormat
        }
        return dateFormatter.string(from: (wrapped as! NSDate) as Date)
    }
    
}


