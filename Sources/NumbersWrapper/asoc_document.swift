//
//  numbers_doc_wrapper.swift
//  numbers_doc_wrapper_swift
//
//  Created by Steve Clarke on 27/06/2015.
//  Copyright © 2015 Steve Clarke. All rights reserved.
//

import Foundation
import AppleScriptObjC
import NumbersDocWrapperObjC

public enum RowCol {
    case row, col
}

public class ASOCDocument {
    static let scriptName = "NumbersAccess.scpt"
    let docPath : String
    let template : String?
    public var script : NumbersAccessOC
    
    required public init(doc: URL?, template: String? = nil) {
        let wrapper = NumbersDocWrapperOC()
        guard let script = wrapper?.script else {
            fatalError("Unable to get script.")
        }
        self.script = script
        if doc == nil && template != nil {
            docPath = "Missing"
            self.template = template
            create()
        } else if doc != nil && template == nil {
            self.template = nil
            self.docPath = doc!.path
            script.openNumbersDoc(docPath)
        } else if  doc != nil && template != nil {
            self.template = template
            self.docPath = doc!.path
            script.openNumbersDoc(docPath)
        } else {
            fatalError("At least one of doc or template is required")
        }
    }
    
    public class func aed(from val: Any) -> NSAppleEventDescriptor? {
        let aed : NSAppleEventDescriptor?
        switch val {
        case is Int:
            aed = NSAppleEventDescriptor(int32: Int32(truncating: val as! NSNumber))
        case is String, is NSString :
            aed = NSAppleEventDescriptor(string: val as! String)
        case is Date, is NSDate :
            aed = NSAppleEventDescriptor(date: val  as! Date)
        case is Bool :
            aed = NSAppleEventDescriptor(boolean: val as! Bool)
        case is Double :
            aed = NSAppleEventDescriptor(double: val as! Double)
        default:
            print("Uncategorized type")
            aed = nil
        }
        return aed
    }
    public func create() {
        print("Creating new document")
        if template == nil { fatalError("Must set template name before using create")}
        script.makeDocument(template!)
    }

    public func open() {
        print("docpath is \(docPath)")
        script.openNumbersDoc(docPath)
    }

    public func exportAsExcel(_ fileName: String) -> () {
        let res : NSNumber = script.asxcel(fileName)
        guard res == 0 else {fatalError("Non-zero resturn from export")}
    }

    public func close(saving: Bool, savingIn: String) {
        script.closeSaving( _: saving as NSNumber, savingIn: savingIn)
    }
    
    public func matrixProperties() throws -> [String: Int] {
        if let dict = script.matrixProperties() {
            return dict as![String : Int]
        } else {
            fatalError("matrix properties are nil")
        }
    }
    
    public func addRowBelow() {
        script.addRowBelow()
    }
    
    public func sheetNames() -> [String] {
        return script.sheetNames() as! [String]
    }
    
    public func activateSheetNamed(_ name: String) -> Bool {
        script.activateSheetNamed(name)
        if script.activeSheetName() as String == name {
            return true
        } else {
            return false
        }
    }
    
    func aeSetVector(_ rowQcol: RowCol, vidx: Int, startAt: Int, values: [Any?]) {
        let rowColChar = NSAppleEventDescriptor(string: rowQcol == RowCol.row ? "R" : "C")
        let aedc = NSAppleEventDescriptor(int32: Int32(truncating: vidx + 1 as NSNumber))
        let aeds = NSAppleEventDescriptor(int32: Int32(truncating: startAt + 1 as NSNumber))
        let aelist = NSAppleEventDescriptor.list()
        for (index,val) in values.enumerated() {
            if val == nil {
                aelist.insert(NSAppleEventDescriptor(string: "--MISSING VALUE--" as String), at: index + 1)
            } else {
                //debugPrint("val is \(val!)")
                if let aed = ASOCDocument.aed(from: val! as Any) {
                    aelist.insert(aed, at: index + 1)
                } else {
                    print("Val: \(val!)")
                    fatalError("Failed to create aed for \(val!)")
                }
            }
        }
        script.aeSetVector(rowColChar, vidx: aedc, startAt: aeds, values: aelist)
    }
    
    public func aeSetCol(_ col: Int, startAt: Int, values: [Any?]) {
        aeSetVector(RowCol.col, vidx: col , startAt: startAt, values: values)
    }

    public func aeSetRow(_ row: Int, startAt: Int, values: [Any?]) {
        aeSetVector(RowCol.row, vidx: row , startAt: startAt, values: values)
    }

    public func updateDate(_ row: Int32, column col: Int32, date: Date) {
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat="dd/MM/yyyy  00:00:00"
        let ds : String = dateFormatter.string(from: date)
        script.updateDate(_ : NSNumber(value: row + 1), column: NSNumber(value: col + 1) , value: ds)
    }

    public func updateNumber(_ row: Int32, column col: Int32, value: NSNumber ) {
        script.update(NSNumber(value: row + 1), column: NSNumber(value: col + 1), value: value)
    }

    public func updateText(_ row: Int32, column col: Int32, text: String) {
        script.updateText(_ : NSNumber(value: row + 1), column: NSNumber(value: col + 1) , value: text)
    }

    public func updateReals(_ rowCol: (Int, Int), values:  [Double]) {
        let valReals : [NSNumber] = values.map() { $0 as NSNumber}
        var row,col : Int
        (row,col) = rowCol
        script.updateReals(_ : [NSNumber(value: row + 1), NSNumber(value: col + 1)] , values: valReals)
    }
    
    public func matrix(_ headers: Int = 1, rowHandler:  @escaping (Int, [ASOCCell]) -> ASOCRow = {ASOCRow($0, cells: $1)}) -> ASOCMatrix{
        return try! ASOCMatrix(headers: headers , cells: script.cells()! as [AnyObject], props: matrixProperties(), rowHandler: rowHandler)
    }
    
    public func headerCount() -> Int {
        return Int(truncating: script.headerCount())
    }
    
    public func rowCount() -> Int {
        guard let rowCount =  script.matrixProperties()?["rowCount"] as? Int else {
            fatalError("Failed to create matrix.  Underlying file/template not found??")
        }
        return rowCount
    }
    
    public func columnCount() -> Int {
        return script.matrixProperties()!["columnCount"]! as! Int
    }
    
    

}
