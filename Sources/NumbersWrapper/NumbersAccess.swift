import Foundation
import CoreFoundation

@objc(NSObject) protocol NumbersAccess {
    func activateSheet(_ : NSNumber)
    func applicationName() -> NSString
    func openNumbersDoc(_ : NSString)
    func makeDocument(_ : NSString)
    func activateSheetNamed( _ : NSString)
    func export( asExcel :NSString) -> NSNumber
    func closeSaving(_ : NSNumber,  savingIn: NSString)
    func updateNumber(_  : NSNumber, column: NSNumber, value: NSNumber )
    func updateText(_ : NSNumber, column: NSNumber ,value: NSString)
    func updateDate(_ : NSNumber , column:  NSNumber, value: NSString )
    func updateReals(_ : NSArray, values: NSArray)
    func aeSetCol(colNum : NSAppleEventDescriptor, startAt: NSAppleEventDescriptor , values: NSAppleEventDescriptor)
    func aeSetRow(rowNum : NSAppleEventDescriptor, startAt: NSAppleEventDescriptor, values: NSAppleEventDescriptor )
    func aeSetVector(_ rowQcol: NSAppleEventDescriptor , vidx: NSAppleEventDescriptor, startAt: NSAppleEventDescriptor, values: NSAppleEventDescriptor)
    func addRowBelow()
    func headerCount() -> NSNumber
    func sheetNames() -> NSArray
    func cells() -> NSArray
    var activeSheetName : NSString {get}
    func matrixProperties() -> NSDictionary?

}

/**
#import <Foundation/Foundation.h>

@protocol NumbersAccessOC <NSObject>

-  (void) activateSheet:  (NSNumber *) sheetIndex;
-  (NSString *) openNumbersDoc: (NSString *) docPath;
-  (void) makeDocument: (NSString *) templateName;
-  (void) activateSheetNamed: (NSString * ) sheetName;
-  (NSNumber *) exportAsExcel: (NSString * ) fileName;
-  (NSString *) applicationName;
-  (void) closeSaving: (NSNumber *) closeOpt savingIn: (NSString *) fileName;
-  (void) updateNumber: (NSNumber *) row column:  (NSNumber *) col value: (NSNumber *) value;
-  (void) updateText: (NSNumber *) row column:  (NSNumber *) col value: (NSString  *) value;
-  (void) updateDate: (NSNumber *) row column:  (NSNumber *) col value: (NSString  *) value;
-  (void) updateReals: (NSArray *) rowCol values: (NSArray *) array;
-  (void) aeSetCol: (NSAppleEventDescriptor *) colNum startAt: (NSAppleEventDescriptor *) start values: (NSAppleEventDescriptor *) list;
-  (void) aeSetRow: (NSAppleEventDescriptor *) rowNum startAt: (NSAppleEventDescriptor *) start values: (NSAppleEventDescriptor *) list;
-  (void) aeSetVector: (NSAppleEventDescriptor *) rowQcol vidx: (NSAppleEventDescriptor *) vidx startAt: (NSAppleEventDescriptor *) start values: (NSAppleEventDescriptor *) list;
-  (void) addRowBelow;
- (NSNumber *) headerCount;
- (NSArray *) sheetNames;
- (NSArray *)  cells;
- (NSString * ) activeSheetName;
- (NSDictionary *) matrixProperties;

@end


@interface NumbersDocWrapperOC : NSObject

@property ( retain) id <NumbersAccessOC>  script ;

- (NumbersDocWrapperOC *) init;

@end
**/

