//
//  asoc_matrix.swift
//  CreditCardStatementToQif
//
//  Created by Steve Clarke on 03/12/2014.
//  Copyright (c) 2014 Steve Clarke. All rights reserved.
//

import Foundation

public typealias RowHandler =  (_ index: Int, _ cells: [ASOCCell]) -> ASOCRow


public class ASOCMatrix : Collection {
    /// Returns the position immediately after the given index.
    ///
    /// - Parameter i: A valid index of the collection. `i` must be less than
    ///   `endIndex`.
    /// - Returns: The index value immediately after `i`.

    public func index(after i: Int) -> Int {
        return i + 1
    }

    
    let headers: Int
    var cells : [AnyObject]
    public let rowCount : Int
    let columnCount : Int
    let rowHandler : RowHandler
    required  public init(headers: Int, cells: [AnyObject], props: [String: Int], rowHandler: @escaping RowHandler = {ASOCRow($0,cells: $1)}){
        self.cells = cells
        self.rowCount = props["rowCount"]! as Int
        self.columnCount = props["columnCount"]! as Int
        self.rowHandler = rowHandler
        self.headers = headers
    }
    
    public var endIndex : Int {return rowCount - 1}
    public var startIndex = 0
    
    public subscript(row : Int) -> ASOCRow {
        var rowArray = [ASOCCell]()
        var cell : ASOCCell
        for cellIndex in (row * columnCount)..<((row + 1) * columnCount) {
            do {
                try cell = ASOCCell.makeCell(self.cells[cellIndex], self.rowIndex(cellIndex), self.colIndex(cellIndex))
                rowArray.append(cell )
            } catch {
                fatalError(("Failed to append row."))
            }
        }
        let newRow = rowHandler(row, rowArray)
        return newRow
    }
    func colIndex(_ cellIndex: Int) -> Int {
        return cellIndex - rowIndex(cellIndex) * columnCount
    }
    func rowIndex(_ cellIndex: Int) -> Int {
        return Int( cellIndex / columnCount)
    }
    
    public func dict() -> [String : ASOCCell] {
        return [Int](headers..<rowCount).reduce([String : ASOCCell]()) { ( dicIn: [String : ASOCCell], idx: Int) -> [String : ASOCCell] in
            var dic :[String : ASOCCell] = dicIn
            dic[self[idx][0].string(.ABORT)] = self[idx][1]
            return dic
        }
    }
    
    public func data(dataHandler: @escaping (_ index: Int, [ASOCCell]) -> ASOCRow = {ASOCRow($0,cells: $1)}) -> ASOCMatrix {
        let selectedCells: [AnyObject]  = cells[columnCount * headers..<cells.count].reduce([AnyObject]()) { ( memIn : [AnyObject], cell: AnyObject) -> [AnyObject] in
            var mem :[AnyObject] = memIn
            mem.append(cell)
            return mem
        }
        let newProps : [String: Int] = ["rowCount" : rowCount - headers, "columnCount" : columnCount ]
        return ASOCMatrix(headers: 0, cells: selectedCells, props: newProps, rowHandler: dataHandler)
    }
}
